<?php snippet('header') ?>

  <main class="main" role="main">

    <header class="wrap">
      <h1><?php echo $page->title()->html() ?></h1>
      <div class="intro text">
        <?php echo $page->text()->kirbytext() ?>
      </div>
    </header>
      
    <div class="wrap">    
      <h2>Projekte</h2>
      <?php snippet('projecttiles', ['tags' => $page->param('tag'), 'classes' => 'wide']);   ?>
    </div>

  </main>

<?php snippet('footer') ?>