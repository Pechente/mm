<?php snippet('header') ?>

  <main class="main" role="main">
    <?
    if(!empty($_POST)):
      $sendername = $_POST['name'];
      $senderadress = $_POST['email'];
      $subject = "Projektanfrage von " . $sendername;
      $message = "Budget: " . $_POST['budget'] . "\nNachricht: \n" . $_POST['message'] . "\nwar auf Seite: " . $_POST['url'];
      $url = $_POST['url'];
      $headers = "From: \"" . $sendername . "\" <" . $senderadress . ">";
      mail("hallo@medienmanufaktur.de", $subject, $message, $headers);
    ?>
    <div id="contactform" class="show">
      <div class="wrap">
        <?php echo $page->text()->kirbytext() ?>
      </div>
      <?php snippet('actionbutton', ['label' => '<i class="fas fa-arrow-left"></i> Zurück zur vorherigen Seite', 'class' => 'messageus wrap', 'url' => $url]) ?>
    </div>
    <?php else: ?>
    <div id="contactform" class="show">
      <div class="wrap">
        <?php echo $page->errortext()->kirbytext() ?>
      </div>
      <?php snippet('actionbutton', ['label' => '<i class="fas fa-arrow-left"></i> Zurück zur Startseite', 'class' => 'messageus wrap', 'url' => $site->url()]) ?>
    </div>
    <?php endif ?>
  </main>

  <footer>
    <div class="textcenter padding-1-tb">
      <a href="<?php echo $site->url() ?>/impressum" class="impressum">Impressum / Datenschutz</a>
    </div>
  </footer>

<?php snippet('barefooter') ?>