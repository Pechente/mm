<?php snippet('header') ?>

  <main class="main" role="main">

    <header>

      <div style="background-color: #eee;" class="team-cover">
        <?php if($slides = $page->background()->toFiles()): ?>
          <?php snippet('slideshow', ['slides' => $slides, 'opacity' => 1]); ?>
        <?php endif ?>
      </div>

      <div class="wrap team-infohead">
        <?php if($image = $page->profilepic()->toFile()): ?>
          <figure class="team-portrait">
            <img src="<?php echo $image->url() ?>" alt="Portrait of <?php echo $page->title()->html() ?>" />
          </figure>
        <?php endif ?>
        <h1><?php echo $page->title()->html() ?></h1>
      </div>

    </header>

    <div class="wrap">
      <div style="font-size: 0">
      <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Kontakt', 'class' => 'messageus width-2', 'url' => 'mailto:' . $page->email()->html()]) ?>
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> '. $page->phone()->html(), 'class' => 'callus width-2', 'url' => 'tel:' . $page->phone()->html()]) ?>
      </div>

      <div>
        <?php echo $page->intro()->kirbytext() ?>
      </div>

      <h2>Skills</h2>
      <ul class="skills">
        <?php $tags = explode('+',param('tag')); ?>
        <?php $skills = explode(",", $page->skills()) ?>
        <?php //print_r($skills) ?>
        <?php //print_r($tags) ?>
        <?php foreach($skills as $skill): ?>      
          <?php foreach($tags as $tag): ?>
            <?php if($skill == $tag): ?>
              <h1>HALLO</h1>
              <li style="color: red"><?php echo $skill ?></li>
              <?php break ?>
            <?php else: ?>
              <li><?php echo $skill ?></li>
            <?php endif ?>
          <?php endforeach ?>
        <?php endforeach ?>
        <?php
        $tags = explode('+',param('tag'));
        if(sizeOf($tags) == 1 && $tags[0] != ''){
            echo "Projekte mit dem Tag: " . $tags[0];
        } else if(sizeOf($tags) > 1) {
            echo "Projekte mit den Tags: ";
            foreach($tags as $i => $tag) {
                $tagsWithOutCurrent = $tags;
                unset($tagsWithOutCurrent[$i]);
                echo '<li><a href="' . $page->url() . '/tag:' . implode('+', $tagsWithOutCurrent) . '">' . $tag . '</a></li>';
            }
        }
        ?>
      </ul>

      <?php if($page->projects()->toPages()->count() > 0): ?>
        <h2>Projekte</h2>
        <?php snippet('projecttiles', ['selectedProjects' => $page->projects()->toPages(), 'classes' => 'wide', 'collection' => $page->id()]);   ?>
      <?php endif ?>
    </div>

  </main>

<?php snippet('footer') ?>
