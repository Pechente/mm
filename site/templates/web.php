<?php snippet('header') ?>

  <main class="main" role="main">
    <header class="whitetext webbg headerbg">
      <?php snippet('slideshow', ['slides' => $page->background()->toFiles(), 'opacity' => 0.2]); ?>
      <div class="wrap textcenter">
        <div class="intro center whitetext">
          <?php echo $page->intro()->kirbytext() ?>
        </div>
        <div class="grid">
        <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Kontakt', 'class' => 'messageus width-2', 'url' => 'mailto:hallo@medienmanufaktur.de']) ?>
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> '. $site->phonegeneraltext(), 'class' => 'callus width-2', 'url' => 'tel:' . $site->phonegeneral()]) ?>
        </div>
      </div>
    </header>

     <div class="wrap wide grid leistungen">

      <div class="wrap"> 
        <h2>Deine neue Website wird</h2>
      </div>

      <?php foreach($page->services()->yaml() as $service): ?>
        <div class="width-3">
        <h3>
          <?php echo $service['service'] ?>
        </h3>
        <a href="mailto:hallo@medienmanufaktur.de?subject=Anfrage Webdesign" class="nohl opencontactform"><img src="<?php echo $page->file($service['icon'][0])->url() ?>" alt="<?php echo $service['service'] ?>"></a>
        <p class="description">
          <?php echo kirbytext($service['description']) ?>
        </p>
      </div>
      <?php endforeach ?>
      <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Projektanfrage', 'class' => 'messageus opencontactform wrap', 'url' => 'mailto:hallo@medienmanufaktur.de?subject=Anfrage Webdesign']) ?>
    </div>

     <div class="beratung wrap">

      <?php echo $page->beratung()->kirbytext(); ?>

      <div class="wrap wide">
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> Jetzt&nbsp;kostenlos beraten&nbsp;lassen', 'class' => 'callus', 'url' => 'tel:' . $site->phonegeneral()]) ?>
      </div>
    </div>

    <?php /*
    <div class="grid brightbg padding-1-tb">
        <div class="wrap textcenter">
            <h2>Wir werden <em>Teil deines Workflows.</em></h2>
            <?php foreach($page->smallservices()->yaml() as $service): ?><div class="width-3">
            <img class="width-60" src="<?php echo $page->images()->find($service['icon'])->url(); ?>">
            <h3>
                <?php echo $service['service'] ?>
            </h3>
            <p class="description">
                <?php echo kirbytext($service['description']); ?>
            </p>
            </div><?php endforeach ?>
        </div>
    </div>
    */ ?>

    <div class="text wrap">
      <?php echo $page->text()->kirbytext() ?>
    </div>
  </main>

<?php snippet('footer') ?>
