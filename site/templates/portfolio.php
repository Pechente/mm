<?php snippet('header') ?>

  <main class="main" role="main">

    <header>

      <?php if($page->background()->toFiles()->count() > 0): ?>
        <div style="background-color: #eee; min-height: 300px; position: relative">
            <?php snippet('slideshow', ['slides' => $page->background()->toFiles(), 'opacity' => 1]); ?>
        </div>
      <?php else: ?>
        <div style="min-height: 100px;"></div> <?php //ein Spacer ?>
      <?php endif ?>

    </header>

    <div class="text wrap">

      <h1><?php echo $page->title()->html() ?></h1>

      <?php echo $page->text()->kirbytext() ?>

      <h2>Projekte</h2>
      <?php snippet('projecttiles', ['selectedProjects' => $page->projects()->toPages(), 'classes' => 'wide', 'collection' => $page->id()]);   ?>

    </div>

  </main>

<?php snippet('footer') ?>
