<?php snippet('header') ?>

  <main class="main" role="main">
    <header class="headerbg whitebg">
      <?php if($slides = $page->background()->toFiles()): ?>
        <?php snippet('slideshow', ['slides' => $slides, 'opacity' => 0.4]); ?>
      <?php endif ?>
      <div class="wrap textcenter">
        <a href="#" rel="home"><img class="lp-logo" src="<?php echo $site->url() ?>/assets/images/mm-logo.svg" alt"<?php echo $site->title()->html() ?> Logo"></a>
        <div class="intro center">
          <?php echo $page->intro()->kirbytext() ?>
        </div>
        <div style="font-size: 0">
        <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Kontakt', 'class' => 'messageus width-2', 'url' => 'mailto:hallo@medienmanufaktur.de']) ?>
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> '. $site->phonegeneraltext(), 'class' => 'callus width-2', 'url' => 'tel:' . $site->phonegeneral()]) ?>
      </div>
      </div>
    </header>

    <div class="philosophy brightbg">
      <div class="wrap">
        <?php echo $page->philosophy()->kirbytext() ?>
      </div>
      <ul class="team wrap">
      <?php foreach($site->find('team')->children()->visible() as $member): ?>
        <li class="team-item thumbnail">
        <a href="<?php echo $member->url() ?>">

          <?php if($image = $member->profilepic()->toFile()): ?>
            <figure class="team-portrait">
              <img src="<?php echo $image->thumb(['width'   => 120, 'quality' => 60])->url() ?>" alt="Portrait of <?php echo $member->title()->html() ?>" />
            </figure>
          <?php endif ?>

          </a>
        </li>
      <?php endforeach ?>
      </ul>
    </div>

    <div class="wrap wide grid leistungen">

    <div class="wrap">
      <h2>Was wir gut können</h2>
    </div>

      <?php foreach($page->leistungen()->yaml() as $leistung): ?>
        <div class="leistung width-3">
        <h3>
          <?php echo $leistung['service'] ?>
        </h3>
        <a href="<?php echo $leistung['url'] ?>" class="nohl"><img src="<?php echo $page->file($leistung['icon'][0])->url() ?>" alt="<?php echo $leistung['service'] ?>"></a>
        <p class="description">
          <?php echo $leistung['description'] ?>
        </p>
        <a href="<?php echo $leistung['url'] ?>" class="mehr">mehr <i class="fas fa-arrow-right"></i></a>
      </div>
      <?php endforeach ?>
    </div>

    <div class="beratung wrap">
      <?php echo $page->text()->kirbytext() ?>

        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> Jetzt&nbsp;kostenlos beraten&nbsp;lassen', 'class' => 'callus', 'url' => 'tel:' . $site->phonegeneral()]) ?>

    </div>

    <div class="home-team brightbg">
      <div class="wrap">
        <h2>Team</h2>
        <?php snippet('team') ?>
      </div>
    </div>

    <section class="home-logos">
      <div class="wrap">
        <h2>Unsere Kunden</h2>
      </div>
      <?php snippet('customers') ?>
    </section>
  </main>

<?php snippet('footer') ?>
