<?php snippet('header') ?>

  <main class="main" role="main">
    <header class="whitetext filmbg headerbg">
      <?php snippet('slideshow', ['slides' => $page->background()->toFiles(), 'opacity' => 0.2]); ?>
      <div class="wrap textcenter">
        <?php /*
        //Temporarily removed
        <div style="margin: 3rem">
            <a class="nohl" href="#"><i class="far fa-play-circle play-symbol"></i></a><br>
            <a href="#">Film Reel ansehen</a>
        </div>
        */?>
        <div class="intro center whitetext">
          <?php echo $page->intro()->kirbytext() ?>
        </div>
        <div class="grid">
        <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Kontakt', 'class' => 'messageus width-2', 'url' => 'mailto:hallo@medienmanufaktur.de']) ?>
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> '. $site->phonegeneraltext(), 'class' => 'callus width-2', 'url' => 'tel:' . $site->phonegeneral()]) ?>
        </div>
      </div>
    </header>

    <div class="film-services">
      <?php foreach($page->services()->yaml() as $service): ?>
        <div class="whitetext animateme" style="background: linear-gradient(to bottom, rgba(0, 0, 0, 0.85) 0%, rgba(0, 0, 0, 0.85) 100%), url('<?php echo $page->images()->find($service['background'][0])->url() ?>') center center;">
          <div class="wrap service">
            <h2>
              <?php echo $service['service'] ?>
            </h2>
            <span class="description">
              <?php echo kirbytext($service['description']) ?>
            </span>
            <a href="mailto:hallo@medienmanufaktur.de?subject=Anfrage <?php echo $service['service'] ?>" class="mehr opencontactform">Anfrage <i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
      <?php endforeach ?>
    </div>

     <div class="beratung wrap">

      <?php echo $page->beratung()->kirbytext(); ?>

      <div class="wrap wide">
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> Jetzt&nbsp;kostenlos beraten&nbsp;lassen', 'class' => 'callus', 'url' => 'tel:' . $site->phonegeneral()]) ?>
      </div>
    </div>

    <div class="grid brightbg mini-leistungen">
        <div class="wrap textcenter">
            <h2>Wir werden Teil deines Workflows</h2>
            <?php foreach($page->smallservices()->yaml() as $service): ?><div class="width-3 mini-leistung animateme">
            <img class="width-60" src="<?php echo $page->images()->find($service['icon'][0])->url(); ?>">
            <h3>
                <?php echo $service['service'] ?>
            </h3>
            <p class="description">
                <?php echo kirbytext($service['description']); ?>
            </p>
            </div><?php endforeach ?>
            <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Jetzt Anfrage senden', 'class' => 'messageus width-2 opencontactform', 'url' => 'mailto:hallo@medienmanufaktur.de?subject=Anfrage Filmproduktion']) ?>
        </div>
    </div>

    <div class="text wrap">
      <?php echo $page->text()->kirbytext() ?>
    </div>
  </main>

<?php snippet('footer') ?>
