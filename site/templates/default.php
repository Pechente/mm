<?php snippet('header') ?>

  <main class="main" role="main">

    <header class="wrap">
      <h1><?php echo $page->title()->html() ?></h1>
      <div class="intro text">
        <?php echo $page->intro()->kirbytext() ?>
      </div>
    </header>
      
    <div class="text wrap">
      <?php echo $page->text()->kirbytext() ?>
    </div>

  </main>

<?php snippet('footer') ?>