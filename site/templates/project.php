<?php
/* ------ Hol' dir das vorige und nächste Projekt in der Kollektion ------*/
$collection = param('collection');
$collectionpage = str_replace('+', '/', $collection); //Slashes wieder einbauen, damit page gültig ist
if($collectionpage && $pages->findByID($collectionpage)) {
  $projects = $pages->findByID($collectionpage)->projects()->toPages(); //get projects collection
  $i = 0;
  $prevProject = "";
  $nextProject = "";
  foreach ($projects as $project) {
    if($page == $project) {
      if( $i > 0 ) {
        $prevProject = $projects->nth($i - 1);
      } 
      if( $i < $projects->count() ) {
        $nextProject = $projects->nth($i + 1);
      }
    }
    $i++;
  }
}
?>

<?php snippet('header') ?>

  <?php if($customcss = $page->css()): ?>
    <style>
      <?php echo $customcss ?>
    </style>
  <?php endif ?>

  <main class="main" role="main">

    <?php if($page->sections()->isEmpty()): ?>

    <header class="mediaframe">

    <?php
    if($page->projectVideoH264()->toFile() && $page->projectVideoWebm()->toFile() && $page->projectImage()->toFile() && $page->autoplay()->isFalse()){
      echo '
        <video id="' . $page->uid() .'-video" poster="' . $page->projectImage()->toFile()->thumb(['width'   => 1920, 'quality' => 80])->url() . '" class="videoplayer plyr" controls playsinline preload>
          <source src="' . $page->projectVideoH264()->toFile()->url() . '" type="video/mp4">
          <source src="' . $page->projectVideoWebm()->toFile()->url() . '" type="video/webm">
        </video>
      ';
    }
    else if($page->projectVideoH264()->toFile() && $page->projectVideoWebm()->toFile() && $page->autoplay()->isTrue()){
      echo '
        <video id="' . $page->uid() .'-video" class="videoloop" autoplay loop muted playsinline>
          <source src="' . $page->projectVideoH264()->toFile()->url() . '" type="video/mp4">
          <source src="' . $page->projectVideoWebm()->toFile()->url() . '" type="video/webm">
        </video>
      ';
    }
    else if($page->projectImage()->toFile()) {
      echo '
        <img class="project-image" src="' . $page->projectImage()->toFile()->url() . '" alt="">
      ';
    }
    ?>

    </header>

    <div class="text wrap">

      <h1><?php echo $page->title()->html() ?></h1>

      <?php echo $page->text()->kirbytext() ?>

    </div>

    <?php
    else:
      foreach($page->sections()->toBuilderBlocks() as $block){
        snippet('sections/' . $block->_key(), array('data' => $block));
      }
    endif;
    ?>

    <div class="credits">
      <div class="wrap">
      <h2>Credits</h2>

      <table class="aligncenter">
      <?php
      //Setting up variables
      $lastroles = "";
      $roleshtml = "";
      $rolesclass = "";
      $rowclass = "";
      //Members Array will later be used for Links to Members
      $members = $site->find('team')->children()->visible();
      ?>
      <?php foreach($page->credits()->yaml() as $credit): ?>
        <?php 
        //ROLES
        $roles = explode(",", $credit['roles']);

        //if the roles are the same as in the entry before, don't print them
        if($lastroles == $roles) {
          $roleshtml = "";
          $rolesclass = " hidemobile"; // die Tabelle verstecken
        }
        else {
          foreach($roles as $role) {
            $roleshtml .= $role . ", ";
          }
          //remove the last comma
          $roleshtml = substr($roleshtml, 0, -2);
          //add :
          $roleshtml = $roleshtml . ":";
          //change TR class for more padding-top;
          $rowclass = "newrole";
        }
        //set current roles to "last roles"
        $lastroles = $roles;

        //NAME
        $creditname = strtolower(trim($credit['name']));
        $namehtml = "";
        foreach($members as $member){
          //echo $creditname . ' / ' . strtolower($member->title()) . ' ---- ';
          if($creditname == strtolower($member->title()) || $creditname == $member->uid()){
            $namehtml = '<a class="team-member" href="' . $member->url() . '"><img src="' . $member->profilepic()->toFile()->thumb(['width'   => 60, 'quality' => 60])->url() . '" alt="' . $member->title() . '"><div class="member-name">' . $member->title() . '</div></a>';
            break; //da sonst Schleife wieder überschrieben wird durch andere Cases (etwas kompliziert)
          }
          //Checke, ob der user ne URL angegeben hat
          else if(!empty($credit['url'])){
            $namehtml = '<a class="non-member" target="_blank" href="' . $credit['url'] . '">' . $credit['name'] . '</a>';
          }
          else {
            $namehtml = '<span class="non-member">' . $credit['name'] . '</span>';
          }
        }
        ?>
        <tr class="<?php echo $rowclass ?>">
          <td class="credits-roles<?php echo $rolesclass ?>"><?php echo $roleshtml ?></td><td class="credits-name"><?php echo $namehtml ?></td>
        </tr>

        <?php $roleshtml = "" //reset after each loop, else it'll pile up?>
        <?php $rolesclass = "" //reset after each loop, else it'll pile up?>
        <?php $rowclass = "" //reset after each loop, else it'll pile up?>
      <?php endforeach ?>
      </table>
      </div>
    </div>

      <?php
      // Images for the "project" template are sortable. You
      // can change the display by clicking the 'edit' button
      // above the files list in the sidebar.
      /*foreach($page->images()->sortBy('sort', 'asc') as $image): ?>
        <figure>
          <img src="<?php echo $image->url() ?>" alt="<?php echo $page->title()->html() ?>" />
        </figure>
      <?php endforeach */?>

    </div>

    <?php //snippet('prevnext') ?>

    <div class="wrap">
        <h2>Möchtest du auch sowas?</h2>
        <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Projektanfrage senden', 'class' => 'messageus opencontactform', 'url' => 'mailto:hallo@medienmanufaktur.de?subject=Interesse an Projekt' . $page->title()]) ?>
    </div>

    <div class="wrap">
      <?php if($page->projects()->toPages()->count() > 0): ?>
        <h2>Ähnliche Projekte</h2>
        <?php snippet('projecttiles', ['selectedProjects' => $page->projects()->toPages(), 'classes' => 'wide']);   ?>
      <?php endif ?>
    </div>

  </main>
  <?php // Bau die Projekt-Navi ?>
  <?php if($collectionpage && $pages->findByID($collectionpage)): ?>
    <nav class="projectnav">
      <div class="wrap wide textcenter">
        <div class="item">
          <?php if($prevProject): ?>
            <a class="prevproject" href="<?php echo $prevProject->url() . "/collection:" . $collection ?>"><?php echo $prevProject->title() ?></a>
          <?php endif ?>
        </div>
        <div class="item">
          <a class="backlink" href="<?php echo $pages->findByID($collectionpage)->url() ?>"><span class="zurueckzu">zurück zu </span><?php echo $pages->findByID($collectionpage)->title() ?></a>
        </div>
        <div class="item">
          <?php if($nextProject): ?>
            <a class="nextproject" href="<?php echo $nextProject->url() . "/collection:" . $collection ?>"><?php echo $nextProject->title() ?></a>
          <?php endif ?>
        </div>
      </div>
    </nav>
  <?php endif ?>

<?php snippet('footer') ?>
