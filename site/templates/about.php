<?php snippet('header') ?>

  <main class="main" role="main">

    <header class="headerbg whitebg">
      <?php if($slides = $page->background()->toFiles()): ?>
        <?php snippet('slideshow', ['slides' => $slides, 'opacity' => 0.2]); ?>
      <?php endif ?>
      <div class="wrap textcenter">
        <?php snippet('team') ?>
        <div class="intro center">
          <?php echo $page->intro()->kirbytext() ?>
        </div>
        <div style="font-size: 0">
      </div>
      </div>
    </header>

    <div class="wrap">

      <div class="text">
        <?php echo $page->text()->kirbytext() ?>
      </div>

    </div>

  </main>

<?php snippet('footer') ?>
