<?php snippet('header') ?>

  <main class="main" role="main">
    <header class="whitetext printbg headerbg">
      <?php snippet('slideshow', ['slides' => $page->background()->toFiles(), 'opacity' => 0.2]); ?>
      <div class="wrap textcenter">
        <div class="intro center whitetext">
          <?php echo $page->intro()->kirbytext() ?>
        </div>
        <div class="grid">
        <?php snippet('actionbutton', ['label' => '<i class="far fa-envelope"></i> Kontakt', 'class' => 'messageus width-2', 'url' => 'mailto:hallo@medienmanufaktur.de']) ?>
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> '. $site->phonegeneraltext(), 'class' => 'callus width-2', 'url' => 'tel:' . $site->phonegeneral()]) ?>
        </div>
      </div>
    </header>

    <?php /*
    <section class="animation print-anim">
      <div class="slide slide-1">
        <h2>Wir bringen Farbe</h2>
        <svg width="80" height="80" class="drop">
          <circle cx="40" cy="40" r="40" fill="#dd59b5" />
        </svg>
      </div>
      <div class="slide slide-2">
        <svg width="1920px" height="1080px" class="bg">
          <path d="M-2.45674507,-807 L1917.54325,-807 C1920.70065,-739.414951 1920.33346,-682.659742 1916.44169,-636.734375 C1910.60404,-567.846324 1887.99283,-511.309939 1874.70732,-499.765625 C1861.42181,-488.221311 1856.19061,-568.257813 1837.637,-568.257813 C1819.0834,-568.257813 1825.711,-464.741336 1800.49638,-499.765625 C1775.28176,-534.789914 1792.45226,-667.53125 1766.16825,-667.53125 C1739.88425,-667.53125 1722.02067,-506.666388 1695.96513,-499.765625 C1669.90959,-492.864862 1667.70609,-631.61607 1649.887,-636.734375 C1632.06792,-641.85268 1641.03286,-501.544456 1617.55107,-522.289063 C1594.06928,-543.033669 1590.99915,-698.590192 1573.34794,-704.351563 C1555.69674,-710.112933 1554.81003,-558.393886 1541.29325,-547.179688 C1527.77648,-535.965489 1530.07281,-660.978818 1504.05107,-646.859375 C1478.02932,-632.739932 1468.74194,-512.438208 1453.91044,-499.765625 C1439.07895,-487.093042 1429.76924,-571.392611 1412.05888,-568.257813 C1394.34852,-565.123014 1403.59522,-488.015625 1387.52763,-488.015625 C1371.46004,-488.015625 1377.38738,-615.095851 1348.19169,-568.257813 C1318.996,-521.419774 1333.10515,-357.96875 1306.46513,-357.96875 C1279.82511,-357.96875 1258.72779,-550.562717 1241.887,-568.257813 C1225.04622,-585.952908 1228.23534,-280.320312 1205.15263,-280.320312 C1182.06992,-280.320312 1186.81767,-408.241493 1151.20732,-383 C1115.59696,-357.758507 1119.0108,-200.9375 1093.16044,-200.9375 C1067.31009,-200.9375 1074.64777,-395.947443 1047.62919,-383 C1020.61062,-370.052557 1014.63814,-172.758632 996.043255,-154.398437 C977.448372,-136.038243 970.317887,-333.67939 943.637005,-280.320312 C916.956123,-226.961235 934.804202,0.8125 918.43388,0.8125 C902.063558,0.8125 895.291445,-267.31822 878.191692,-280.320312 C861.09194,-293.322405 854.795792,-69.7809073 841.004192,-58.0625 C827.212593,-46.3440927 819.571614,-211.95411 814.05888,-225.828125 C808.546145,-239.70214 811.869393,-145.454256 795.449505,-172.710937 C779.029617,-199.967619 784.195913,-307.63854 767.71513,-302.757813 C751.234347,-297.877085 755.276367,-130.089619 733.613567,-154.398437 C711.950767,-178.707256 723.087164,-538.926027 698.097942,-522.289063 C673.108721,-505.652098 672.849139,-305.502962 643.801067,-327.5 C614.752996,-349.497038 610.015878,-516.466725 595.90263,-547.179688 C581.789382,-577.89265 576.170121,-280.320312 558.74638,-280.320312 C541.322639,-280.320312 545.217009,-494.635443 522.730755,-461.601562 C500.244501,-428.567682 506.103683,-185.07532 485.49638,-172.710937 C464.889077,-160.346555 445.828913,-387.542975 430.738567,-406.40625 C415.648222,-425.269525 421.847074,-264.044021 399.410442,-280.320312 C376.973811,-296.596604 380.549926,-484.983687 354.65263,-461.601562 C328.755333,-438.219438 333.282865,-422.612824 313.68388,-406.40625 C294.084895,-390.199676 286.832203,-588.46875 261.793255,-588.46875 C236.754306,-588.46875 232.80906,-412.320538 213.480755,-406.40625 C194.152449,-400.491962 196.976455,-557.146057 178.074505,-562.851562 C159.172555,-568.557068 152.859123,-431.296875 131.019817,-431.296875 C109.180512,-431.296875 106.678489,-552.649153 90.5901299,-562.851562 C74.5017712,-573.053972 70.8957228,-478.512724 45.0901299,-485.789062 C19.284537,-493.065401 3.69686446,-542.5 -0.261432566,-588.46875 C-2.90029725,-619.114583 -3.63206808,-691.958333 -2.45674507,-807 Z" id="Rectangle" fill="#D8D8D8"></path>
        </svg>
      </div>
    </section>
    
    */ ?>
    <?php foreach($page->services()->yaml() as $service): ?>
      <div style="background: linear-gradient(to bottom, rgba(255, 255, 255, 0.85) 0%, rgba(255, 255, 255, 0.85) 100%), url('<?php echo $page->images()->find($service['background'][0])->url()?>') center center;">
      <div class="service wrap">
        <h2>
          <?php echo $service['service'] ?>
        </h2>
        <span class="description">
          <?php echo kirbytext($service['description']) ?>
        </span>
        <a href="mailto:hallo@medienmanufaktur.de?subject=Anfrage <?php echo $service['service'] ?>" class="mehr opencontactform">Anfrage <i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <?php endforeach ?>

     <div class="beratung wrap">

      <?php echo $page->beratung()->kirbytext(); ?>

      <div class="wrap wide">
        <?php snippet('actionbutton', ['label' => '<i class="fas fa-phone"></i> Jetzt&nbsp;kostenlos beraten&nbsp;lassen', 'class' => 'callus', 'url' => 'tel:' . $site->phonegeneral()]) ?>
      </div>
    </div>

    <?php /*
    <div class="grid brightbg padding-1-tb">
        <div class="wrap textcenter">
            <h2>Wir werden <em>Teil deines Workflows.</em></h2>
            <?php foreach($page->smallservices()->yaml() as $service): ?><div class="width-3">
            <img class="width-60" src="<?php echo $page->images()->find($service['icon'])->url(); ?>">
            <h3>
                <?php echo $service['service'] ?>
            </h3>
            <p class="description">
                <?php echo kirbytext($service['description']); ?>
            </p>
            </div><?php endforeach ?>
        </div>
    </div>
    */ ?>

    <div class="text wrap">
      <?php echo $page->text()->kirbytext() ?>
    </div>
  </main>

<?php snippet('footer') ?>
