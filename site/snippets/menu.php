<nav class="navigation" id="mainnav" role="navigation">
  <ul class="menu">
      <?php foreach($site->children()->visible() as $item): ?>
        <li class="menu-item<?php echo r($item->isOpen(), ' is-active') ?>">
          <a href="<?php echo $item->url() ?>"><?php echo $item->title()->html() ?></a>
        </li>
      <?php endforeach ?>
  </ul>
</nav>
<a href="#" onclick="toggleNav(event)" class="burger-button">
  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
      <g class="rotate-1">
        <rect class="move-1" x="0" y="0" width="26" height="3" />
      </g>
      <rect class="move-2" x="0" y="11.5" width="26" height="3" />
      <g class="rotate-3">
        <rect class="move-3" x="0" y="23" width="26" height="3" />
      </g>
  </svg>
</a>
