<?php

/*

The $flip parameter can be passed to the snippet to flip
prev/next items visually:

```
<?php snippet('prevnext', ['flip' => true]) ?>
```

Learn more about snippets and parameters at:
https://getkirby.com/docs/templates/snippets

*/

$directionPrev = @$flip ? 'right' : 'left';
$directionNext = @$flip ? 'left'  : 'right';

if($page->hasNextVisible() || $page->hasPrevVisible()): ?>
  <nav class="pagination <?php echo !@$flip ?: ' flip' ?> wrap cf">

    <?php if($page->hasPrevVisible()): ?>
      <a class="pagination-item <?php echo $directionPrev ?>" href="<?php echo $page->prevVisible()->url() ?>" rel="prev" title="<?php echo $page->prevVisible()->title()->html() ?>">
        <?php echo (new Asset("assets/images/arrow-{$directionPrev}.svg"))->content() ?>
      </a>
    <?php else: ?>
      <span class="pagination-item <?php echo $directionPrev ?> is-inactive">
        <?php echo (new Asset("assets/images/arrow-{$directionPrev}.svg"))->content() ?>
      </span>
    <?php endif ?>

    <?php if($page->hasNextVisible()): ?>
      <a class="pagination-item <?php echo $directionNext ?>" href="<?php echo $page->nextVisible()->url() ?>" rel="next" title="<?php echo $page->nextVisible()->title()->html() ?>">
        <?php echo (new Asset("assets/images/arrow-{$directionNext}.svg"))->content() ?>
      </a>
    <?php else: ?>
      <span class="pagination-item <?php echo $directionNext ?> is-inactive">
        <?php echo (new Asset("assets/images/arrow-{$directionNext}.svg"))->content() ?>
      </span>
    <?php endif ?>

  </nav>
<?php endif ?>