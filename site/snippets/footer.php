    <footer>
      <div class="wrap grid">

        <h2>Du&nbsp;findest uns&nbsp;in</h2>
        <div>
          <div class="width-2">
            <h3>Bremen</h3>
            <a href="mailto:hallo@medienmanufaktur.de"><i class="far fa-envelope"></i> Kontakt</a>&nbsp;
            <a href="tel:<?php echo $site->phonebremen() ?>"><i class="fas fa-phone"></i> <?php echo $site->phonebrementext() ?></a>
          </div>
          <div class="width-2">
            <h3>Bonn</h3>
            <a href="mailto:hallo@medienmanufaktur.de"><i class="far fa-envelope"></i> Kontakt</a>&nbsp;
            <a href="tel:<?php echo $site->phonebonn() ?>"><i class="fas fa-phone"></i> <?php echo $site->phonebonntext() ?></a>
          </div>
        </div>
      </div>
      <div class="textcenter padding-1-tb">
        <a href="<?php echo $site->url() ?>/impressum" class="impressum">Impressum / Datenschutz</a>
      </div>
    </footer>

  </div><?php // End Pagewrap ?>

  <div id="contactform">
    <div class="wrap wide">
      <a href="#" onclick="toggleContactForm(event)" class="close"><img src="<?php echo $site->url() ?>/assets/images/close.svg" alt="Schließen"></a>
    </div>
    <div class="wrap">
      <h2>Projektanfrage</h2>
      <h3>bitte ausfüllen</h3>
      <form method="post" action="<?php echo $site->url() ?>/kontakt">
        <input type="text" name="name" class="width-2" placeholder="Dein Name*" required>
        <input type="email" name="email" class="width-2" placeholder="Deine E-Mailadresse*" required>
        <span class="width-2 contactform-budget">Budget: </span>
        <select name="budget" class="width-2" required>
          <option value="keine Angabe">keine Angabe</option>
          <option value="1000€ - 3000€">1000€ - 3000€</option>
          <option value="3000€ - 6000€" selected>3000€ - 6000€</option>
          <option value="6000€ - 10.000€">6000€ - 10.000€</option>
          <option value="10.000€+">10.000€+</option>
        </select>
        <textarea type="textarea" name="message" rows="8" class="width-1" placeholder="Beschreibe deine Idee so genau wie möglich, verlinke gerne auf Arbeiten von uns oder anderen zur Veranschaulichung…*" required></textarea>
        <input type="hidden" name="url" value="<?php echo $page->url() ?>">
        <span class="width-2" style="font-size: 0.8rem">*diese Felder müssen ausgefüllt werden</span>
        <input type="submit" class="actionbutton" value="Absenden">
      </form>
    </div>
  </div>

  <!-- Scripts -->
  <?php //JS ?>
  <?php echo js('assets/index.js?ver=' . $site->ver(), true) ?>
  <!-- Matomo -->
  <script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="//tracking.medienmanufaktur.de/";
      _paq.push(['setTrackerUrl', u+'matomo.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <noscript><p><img src="//tracking.medienmanufaktur.de/matomo.php?idsite=1&amp;rec=1" style="border:0;" alt="" /></p></noscript>
  <!-- End Matomo Code -->

</body>
</html>
