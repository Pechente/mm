<ul class="logos">
    <?php foreach($page->customers()->yaml() as $customer): ?>
        <?php
        $widthHTML = '';
        if($width = $customer['width']) {
            $widthHTML = ' style="width: ' . $width . '%;"';
        } 
        ?>
        <li>
            <img src="<?php echo $page->image($customer['icon'][0])->url() ?>" alt="<?php echo $customer['name'] ?>"<?php echo $widthHTML ?>>
        </li>
    <?php endforeach; ?>
</ul>