<?php
$css = "";
$html = "";
$i = 0; //will be counted in the loop
$slideduration = 6; //one slide will stay on for 6s
$fadeduration = 5; //in %!!!

if($slides->count() == 1) {
    $html .= "<figure class='slide-1'></figure>";
    $css .= "
        .slideshow-wrapper figure {
            opacity: 1;
            background: url(" . $slides->first()->thumb(['width'   => 1200, 'quality' => 80])->url() . ") no-repeat center center;
            background-size: cover;
        }
    ";
}
else if($slides->count() > 1) {
    $css .= "
        .slideshow-wrapper figure {
            animation: slideShow " . $slides->count() * $slideduration . "s linear infinite 0s;
        }
    ";
    //Durch die Slides iterieren, um entsprechendes HTML + CSS zu generieren
    foreach($slides as $slide) {
        $css .= "
            .slideshow-wrapper figure.slide-" . $i . " {
                animation-delay: " . $i * $slideduration . "s;
                background: url(" . $slide->thumb(['width'   => 1200, 'quality' => 80])->url() . ") no-repeat center center;
                background-size: cover;
            }
        ";
        $html .= "
            <figure class='slide-" . $i . "'></figure>
        ";
        $i++;
    }
    $css.= "
        @keyframes slideShow {  
            0% {
                opacity: 0;
                transform: scale(1);
            }
            " . 20 / $slides->count() . "% {
                opacity: 1
            }
            " . 100 / $slides->count() . "% {
                opacity: 1;
            }
            " . 120 / $slides->count() . "% {
                opacity: 0;
                transform: scale(1.1);
            }
            100% {
                opacity: 0;
                transform: scale(1);
            }
        }
        
    }";
}
?>
<style>
.slideshow-wrapper {
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  overflow: hidden;
  opacity: <?php echo $opacity ?>;
}
.slideshow-wrapper figure {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  margin: 0;
}
<?php echo $css ?>
</style>
<div class="slideshow-wrapper">
    <?php echo $html ?>
</div>