<?php
$title = $page->title()->html();
if($page->seotitle()->isNotEmpty()) $title = $page->seotitle()->html();

$seocover = "";
if($page->seocover()->isNotEmpty()) $seocover = $page->seocover()->toFile()->url();
else if($page->coverImage()->isNotEmpty()) $seocover = $page->coverImage()->toFile()->url();
else if($page->images()->first()) $seocover = $page->images()->first()->url();

$seodescription = "";
if($page->seodescription()->isNotEmpty()) $seodescription = $page->seodescription();
else if($page->text()->isNotEmpty()) $seodescription = $page->text()->excerpt(160);
else if($site->description()->isNotEmpty()) $seodescription = $site->description()->excerpt(160);

$twitteruser = "Pechente";
if ($site->twittername()->isNotEmpty()) $twitteruser = $site->twittername();

$bodyClass = $page->intendedTemplate();

//Todo: das hier eleganter koten ey
$seolang = "de_de";
$langhtml = "de";
if($page->seolang() == "en_us") {
  $seolang = "en_us";
  $langhtml = "en";
}
?>

<!doctype html>
<html lang="<?php echo $langhtml ?>">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title><?php echo $title ?> | <?php echo $site->title()->html() ?></title>
  <meta name="description" content="<?php echo $seodescription ?>">

  <?php // Favicon ?>
  <link rel="icon" href="<?php echo $site->url() ?>/assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="<?php echo $site->url() ?>/assets/images/favicon.svg" type="image/svg+xml">
  <link rel="canonical" href="<?php echo $page->url() ?>" />

  <?php // CSS ?>
  <?php echo css('assets/index.css?ver=' . $site->ver()) ?>

  <?php // disable indexing for hidden page ?>
  <?php if( ($page->intendedTemplate() == 'project' || $page->intendedTemplate() == 'portfolio') && $page->isInvisible() ): ?>
  <meta name="robots" content="noindex, nofollow">
  <?php else: ?>
  <meta name="robots" content="index, follow">
  <?php endif ?>

  <?php // OG stuffles ?>
  <meta property="og:title" content="<?php echo $title ?>" />
  <meta property="og:locale" content="<?php echo $seolang ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo $page->url() ?>" />
  <meta property="og:image" content="<?php echo $seocover ?>" />
  <meta property="og:description" content="<?php echo $seodescription ?>" />
  <meta property="og:site_name" content="<?php echo $site->title()->html() ?>" />

  <?php // Twitter shit stuffles ?>
  <meta name="twitter:title" content="<?php echo $title ?>" />
  <meta name="twitter:image" content="<?php echo $seocover ?>" />
  <meta name="twitter:description" content="<?php echo $seodescription ?>" />
  <meta name=”twitter:site” content=”<?php echo "@" . $twitteruser ?>”>
  <meta name=”twitter:creator” content=”<?php echo "@" . $twitteruser ?>”>

  <?php // Show elements that are supposed to be animated - Fallback für nojs ?>
  <noscript>
      <style type="text/css">
          .animateme {
            opacity: 1 !important;
          }
      </style>
  </noscript>

</head>
<body class="<?php echo $bodyClass ?>">

  <div id="pagewrap">

    <header class="topnav header wrap wide" role="banner">

        <div class="branding">
          <a href="<?php echo url() ?>" rel="home"><img class="logo" src="<?php echo $site->url() ?>/assets/images/mm-logo-simpel.svg" alt="<?php echo $site->title()->html() ?> Logo"></a> <?php //$site->title()->html() ?>
        </div>

        <?php snippet('menu') ?>

    </header>
