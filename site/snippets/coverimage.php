<?php if($image = $item->coverimage()->toFile()): ?>
  <figure>
    <img src="<?php echo $image->url() ?>" alt="" />
  </figure>
<?php endif ?>
