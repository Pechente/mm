<ul class="team-list grid gutter-1">
  <?php foreach($site->find('team')->children()->visible() as $member): ?>
    <li class="team-item column">

      <a href="<?php echo $member->url() ?>">

        <?php if($image = $member->profilepic()->toFile()): ?>
          <figure class="team-portrait">
            <img src="<?php echo $image->thumb(['width'   => 200, 'quality' => 60])->url() ?>" alt="Portrait of <?php echo $member->title()->html() ?>" />
          </figure>
        <?php endif ?>

        </a>

        <div class="team-info">
          <?php
            //Hol dir den Vornamen
            $firstname = explode(' ',trim($member->title()));
            $firstname = $firstname[0]
          ?>
          <a class="team-name" href="<?php echo $member->url() ?>"><?php echo $firstname ?></a>
        </div>

      </a>

    </li>
  <?php endforeach ?>
</ul>
