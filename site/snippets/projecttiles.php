<?php
function renderProjectTile($uid, $category, $title, $url, $coverimageurl, $firsttag) {
    //set gradient colors
    $gradColor1 = 'rgba(120,120,120,0.8)';
    $gradColor2 = 'rgba(255,255,255,0.8)';
    if($category == 'film'){
        $gradColor1 = 'rgba(196, 107, 255, 0.8)';
        $gradColor2 = 'rgba(65, 135, 255, 0.8)';
    } else if ($category == 'print'){
        $gradColor1 = 'rgba(186, 65, 255, 0.8)';
        $gradColor2 = 'rgba(255, 113, 107, 0.8)';
    } else if ($category == 'web'){
        $gradColor1 = 'rgba(211, 0, 0, 0.8)';
        $gradColor2 = 'rgba(242, 215, 0, 0.8)';
    }

    $output = '
    <a class="projecttile" href="' . $url . '" style="background: linear-gradient(135deg,' .$gradColor1 .' 0%,'. $gradColor2 . ' 100%), url(' . $coverimageurl . '); background-size: cover; background-position: center center;">
        <h3>' . $title . '</h3>
        <span class="category">' . $firsttag . '</span>
    </a>
    ';
    return $output;
}
//Check if it's a collection
$addToURL = "";
if( isset($collection) ) {
    $addToURL = "/collection:" . str_replace('/', '+', $collection);
}

//get params
 /*
$tags = explode('+',param('tag'));
if(sizeOf($tags) == 1 && $tags[0] != ''){
    echo "Projekte mit dem Tag: " . $tags[0];
} else if(sizeOf($tags) > 1) {
    echo "Projekte mit den Tags: ";
    foreach($tags as $i => $tag) {
        $tagsWithOutCurrent = $tags;
        unset($tagsWithOutCurrent[$i]);
        echo '<li><a href="' . $page->url() . '/tag:' . implode('+', $tagsWithOutCurrent) . '">' . $tag . '</a></li>';
    }
} */

if(!$classes) $classes = "";

// Gib die Projekte aus
echo '<div class="projecttiles ' . $classes . '">';
foreach($selectedProjects as $project){
    $coverimageurl = "";
    if($coverimage = $project->coverfiles()->toFile()) {
        $coverimageurl = $coverimage->thumb(['width'   => 500, 'quality' => 80])->url();
    }
    $firsttag = "";
    if($tags = $project->tags()) {
        $tagsarray = explode(',', $tags);
        $firsttag = $tagsarray[0];
    }
    echo renderProjectTile($project->uid(), $project->parent()->uid(), $project->title(), $project->url() . $addToURL, $coverimageurl, $firsttag);
}
echo '</div>';
?>