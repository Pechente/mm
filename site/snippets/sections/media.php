<section class="mediaframe">

<?php
if($data->projectVideoH264()->toFile() && $data->projectVideoWebm()->toFile() && $data->projectImage()->toFile() && $data->autoplay()->isFalse()){
  echo '
    <video id="' . $data->uid() .'-video" poster="' . $data->projectImage()->toFile()->thumb(['width'   => 1920, 'quality' => 80])->url() . '" class="videoplayer plyr" controls playsinline preload>
      <source src="' . $data->projectVideoH264()->toFile()->url() . '" type="video/mp4">
      <source src="' . $data->projectVideoWebm()->toFile()->url() . '" type="video/webm">
    </video>
  ';
}
else if($data->projectVideoH264()->toFile() && $data->projectVideoWebm()->toFile() && $data->autoplay()->isTrue()){
  echo '
    <video id="' . $data->uid() .'-video" class="videoloop" autoplay loop muted playsinline>
      <source src="' . $data->projectVideoH264()->toFile()->url() . '" type="video/mp4">
      <source src="' . $data->projectVideoWebm()->toFile()->url() . '" type="video/webm">
    </video>
  ';
}
else if($data->projectImage()->toFile()) {
  echo '
    <img class="project-image" src="' . $data->projectImage()->toFile()->url() . '" alt="">
  ';
}
?>

</section>