<section class="print-specs">
    <div class="wrap">
        <h2>Farben</h2>
        <?php if( $data->colors() ): ?>
            <ul class="colors">
                <?php foreach($data->colors()->yaml() as $color): ?>
                    <li class="color" style="background: #<?php echo $color ?>"></li>
                <?php endforeach ?>
            </ul>
        <?php endif ?>
    </div>
</section>