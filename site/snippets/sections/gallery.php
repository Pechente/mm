<section class="gallery">
    <?php if( $title = $data->title() ): ?>
        <div class="wrap">
            <h2><?php echo $title ?></h2>
        </div>
    <?php endif ?>
    <?php if( $text = $data->text() ): ?>
        <div class="wrap">
            <p><?php echo $text->html() ?></p>
        </div>
    <?php endif ?>
    <div class="wrap wide">
        <ul class="gallery animateme">
            <?php foreach($data->images()->toFiles() as $image): ?>
                <li>
                    <?php if($data->showIndex()->isTrue()): ?>
                        <div class="index"><?php echo $image->indexOf() //indexOf() ist wichtig, da die Position sich nicht verändert, wenn neue Dateien dazu kommen oder man alte löscht ?></div>
                    <?php endif ?>
                    <img src="<?php echo $image->thumb(['width'   => 1200, 'quality' => 80])->url() ?>" alt="<?php echo $image->name() ?>">
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>