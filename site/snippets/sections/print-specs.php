<section class="print-specs">
    <div class="wrap">
        <h2>Maße</h2>
        <?php if( $data->width() && $data->height() && $data->unit() ): ?>
            <?php $paddingBottom = $data->height()->toInt() / $data->width()->toInt() * 100; ?>
            <?php if( $data->twosided()->isTrue() ): ?>
                <?php $twosided = "beidseitig bedruckt" ?>
            <?php else: ?>
                <?php $twosided = "einseitig bedruckt" ?>
            <?php endif ?>
            <div class="page animateme" style="padding-bottom: <?php echo $paddingBottom ?>%">
                <div class="specs">
                    <?php echo $data->width() . " " . $data->unit() . "<span class='by'> × </span>" . $data->height() . " " . $data->unit() ?>
                    <div class="twosided"><?php echo $twosided ?></div>
                </div>
            </div>
        <?php endif ?>
        <?php if( $text = $data->text() ): ?>
            <p><?php echo $text->kirbytext() ?></p>
        <?php endif ?>
    </div>
</section>