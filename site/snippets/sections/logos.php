<section class="home-logos brightbg">
    <?php if( $title = $data->title() ): ?>
        <div class="wrap">
            <h2><?php echo $title ?></h2>
        </div>
    <?php endif ?>
        <ul class="logos">
            <?php foreach($data->logos()->yaml() as $logo): ?>
                <?php
                $widthHTML = '';
                if($width = $logo['width']) {
                    $widthHTML = ' style="width: ' . $width . '%;"';
                } 
                ?>
                <li>
                    <img src="<?php echo $page->image($logo['icon'][0])->url() ?>" alt="<?php echo $logo['name'] ?>"<?php echo $widthHTML ?>>
                </li>
            <?php endforeach; ?>
        </ul>
</section>