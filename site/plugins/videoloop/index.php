<?php
Kirby::plugin('mm/videoloop', [
    'tags' => [
        'videoloop' => [
            'html' => function($tag) {

                $filename = basename($tag->attr('videoloop'), '.mp4');
                $video = $tag->parent()->files()->find($filename . '.mp4');
                $webm = $tag->parent()->files()->find($filename . '.webm');
            
                return '<video class="videoloop" autoplay loop muted playsinline>
                  <source src="' . $video->url(). '" type="video/mp4">
                  <source src="' . $webm->url() . '" type="video/webm">
                </video>';
              }
        ]
    ]
]);
?>