const { src, dest, watch, series, parallel } = require('gulp');
const scss = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const minifyJS = require('gulp-jsmin');
const concat = require('gulp-concat');
const cache = require('gulp-cache');

const browserSync = require('browser-sync');
const server = browserSync.create();

function reload(done) {
  server.reload();
  done();
}

function serve(done) {
  server.init({
    proxy: "localhost/mm",
    notify: false,
  });
  done();
}

function css() {
    return src('assets/css/*.scss')
      .pipe(scss())
      .pipe(minifyCSS())
      .pipe(concat('index.css'))
      .pipe(dest('assets'))
      .pipe(cache.clear())
}
  
function js() {
    return src('assets/js/**/*.js', { sourcemaps: true })
      .pipe(minifyJS())
      .pipe(concat('index.js'))
      .pipe(dest('assets', { sourcemaps: true }))
      .pipe(cache.clear())
}

watch('assets/css/*.scss', { events: 'all' }, series(css, reload) );
watch('assets/js/*.js', { events: 'all' }, series(js, reload) );
watch('site/**/*.php', { events: 'all' }, series(reload) );

exports.js = js;
exports.css = css;
exports.default = series(css, js, serve);